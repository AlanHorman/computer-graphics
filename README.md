#  Ray Tracing in One Weekend 

## Table of Contents  
- [Introduction](#introduction) 
- [Notes](#notes)

## Introduction

![Image 16: A hollow glass sphere](https://raytracing.github.io/images/img-1.16-glass-hollow.png)

During the winter semester 2019 in the Computer Graphics course of the [Master Applied Computer Science program](https://www.hs-fulda.de/en/studies/departments/applied-computer-science/study-programmes/study-programmes/msc-in-applied-computer-science) at [Fulda University of Applied Sciences](https://www.hs-fulda.de/en/home), 
we had to use C++ to understand and recreate the ray tracing techniques from  [Peter Shirley's book Ray Tracing in One Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) (Chapter 1 - 10).
Microsoft Visual Studio 2015 was used as the IDE.

## Notes
* If you like to learn more about Ray Tracing check out the other [**books**](https://raytracing.github.io/) of Peter Shirley.
* The project contains the ray tracer from [chapter 10](https://raytracing.github.io/books/RayTracingInOneWeekend.html#dielectrics).