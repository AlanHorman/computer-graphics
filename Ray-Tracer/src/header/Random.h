#ifndef RANDOMH
#define RANDOMH

#include <cstdlib>

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

double random_double()
{
    // RAND_MAX betr�gt 32767 
    // Mit rand() erh�lt man eine Zahl zwischen 0 und RAND_MAX
    return rand() / (RAND_MAX + 1.0);
}



#endif