#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <float.h>
#include <cmath>
#include "Color.h"
#include "Camera.h"
#include "Sphere.h"
#include "Material.h"
#include "HitTableList.h"
#include "PositionCamera.h"

/*
    Authors: Ram�n Wilhelm & Oliver Kovarna
*/

/*
    2.1 Write out or Display Image and
    2.2 Simple Pinhole Camera
*/

static Vec3 MetallColor(const Ray& r, HitTable* world, int depth)
{
    // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.

    hit_record rec;

    // Using 0.001 for avoiding the shadow acne problem
    if (world->hit(r, 0.001, FLT_MAX, rec))
    {
        Ray scattered;
        Vec3 attenuation;


        if (depth < 50 && rec.matr_ptr->scatter(r, rec, attenuation, scattered)) {
            return attenuation * MetallColor(scattered, world, depth + 1);
        }
        else {
            return Vec3(0, 0, 0);
        }
    }
    else
    {
        // Hintergrund
        Vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5 * (unit_direction.y() + 1.0);
        // Beide Vektoren sind Farbwerte
        return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
        //return Vec3(1, 1, 1);
    }
}

static Vec3 Materialcolor(const Ray& r, HitTable* world)
{
    // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.
    hit_record rec;

    // Using 0.001 for avoiding the shadow acne problem
    if (world->hit(r, 0.001, FLT_MAX, rec))
    {
        // Berechnet Farbwert f�r diesen Punkt, mithilfe der Normale.
        Vec3 target = rec.p + rec.normal + random_in_unit_sphere();
        return 0.5 * Materialcolor(Ray(rec.p, target - rec.p), world);
    }
    else
    {
        // Hintergrund
        Vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5 * (unit_direction.y() + 1.0);
        // Beide Vektoren sind Farbwerte
        //return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
        return Vec3(1, 1, 1);
    }
}

void feature_2_1_2_2()
{
    int nx = 200;
    int ny = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_1_2.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));

    for (int j = ny - 1; j >= 0; j--) // Zeilen
    {
        for (int i = 0; i < nx; i++)  // Spalten
        {
            float u = float(i) / float(nx);
            float v = float(j) / float(ny);

            Ray ray(cam.origin, cam.lower_left_corner + 
                u * cam.horizontal + v * cam.vertical);

            Vec3 col = Color::color(ray);

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }

}

/*
    2.3 Sphere Intersection
*/

void feature_2_3()
{
    int nx = 200;
    int ny = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_3.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));
    Sphere sphere(Vec3(0, 0, -1), 0.5);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            float u = float(i) / float(nx);
            float v = float(j) / float(ny);

            Ray ray(cam.origin, cam.lower_left_corner +
                u * cam.horizontal + v * cam.vertical);

            Vec3 col = Color::color(sphere.getCenter(), sphere.getRadius(), ray);

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}


/*
    2.4 Multiple Objects
*/

void feature_2_4()
{
    int nx = 200;
    int ny = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_4.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));
    
    HitTable* list[2];
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5);
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100);
    HitTable* world = new HitTableList(list, 2);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            float u = float(i) / float(nx);
            float v = float(j) / float(ny);

            Ray ray(cam.origin, cam.lower_left_corner +
                u * cam.horizontal + v * cam.vertical);

            Vec3 col = Color::colorNormal(ray, world);

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}



/*
    2.5 Supersampling
*/

void feature_2_5()
{
    int nx = 200;
    int ny = 100;
    int ns = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_5.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));

    HitTable* list[2];
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5);
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100);
    HitTable* world = new HitTableList(list, 2);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            Vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                float u = float(i + random_double()) / float(nx);
                float v = float(j + random_double()) / float(ny);

                Ray ray(cam.origin, cam.lower_left_corner +
                    u * cam.horizontal + v * cam.vertical);

                col += Color::colorNormal(ray, world);

            }
            col /= float(ns);

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}


/*
    2.6 Diffuse Material
*/

void feature_2_6()
{
    int nx = 200;
    int ny = 100;
    int ns = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_6.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));

    HitTable* list[2];
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5);
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100);
    HitTable* world = new HitTableList(list, 2);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            Vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                float u = float(i + random_double()) / float(nx);
                float v = float(j + random_double()) / float(ny);

                Ray ray(cam.origin, cam.lower_left_corner +
                    u * cam.horizontal + v * cam.vertical);

                col += Materialcolor(ray, world);

            }
            col /= float(ns);
            col = Vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}

/*
    2.7 Metal Material
*/

void feature_2_7()
{
    int nx = 200;
    int ny = 100;
    int ns = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_7.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));

    HitTable* list[4];
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5, new Lambertian(Vec3(0.8, 0.3, 0.3)));
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100, new Lambertian(Vec3(0.8, 0.8, 0.0)));
    list[2] = new Sphere(Vec3(1, 0, -1), 0.5, new Metal(Vec3(0.8, 0.6, 0.2), 0.3));
    list[3] = new Sphere(Vec3(-1, 0, -1), 0.5, new Metal(Vec3(0.8, 0.8, 0.8), 1.0));
    HitTable* world = new HitTableList(list, 4);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            Vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                float u = float(i + random_double()) / float(nx);
                float v = float(j + random_double()) / float(ny);

                Ray ray(cam.origin, cam.lower_left_corner +
                    u * cam.horizontal + v * cam.vertical);

                col += MetallColor(ray, world, 0);

            }
            col /= float(ns);
            col = Vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}

/*
    2.8 Dielectric Material
*/

void feature_2_8()
{
    int nx = 200;
    int ny = 100;
    int ns = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_8.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    Camera cam(Vec3(-2, -1, -1), Vec3(4, 0, 0), Vec3(0, 2, 0), Vec3(0, 0, 0));

    HitTable* list[5];
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5, new Lambertian(Vec3(0.1, 0.2, 0.5)));
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100, new Lambertian(Vec3(0.8, 0.8, 0.0)));
    list[2] = new Sphere(Vec3(1, 0, -1), 0.5, new Metal(Vec3(0.8, 0.6, 0.2), 0));
    list[3] = new Sphere(Vec3(-1, 0, -1), 0.5, new Dielectric(1.5));
    list[4] = new Sphere(Vec3(-1, 0, -1), -0.45, new Dielectric(1.5));
    HitTable* world = new HitTableList(list, 5);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            Vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                float u = float(i + random_double()) / float(nx);
                float v = float(j + random_double()) / float(ny);

                Ray ray(cam.origin, cam.lower_left_corner +
                    u * cam.horizontal + v * cam.vertical);

                col += MetallColor(ray, world, 0);

            }
            col /= float(ns);
            col = Vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}

/*
    2.9 Better Pinhole Camera
*/

void feature_2_9()
{
    int nx = 200;
    int ny = 100;
    int ns = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/feature002_9.ppm", std::ios::out);
    ofs << "P3\n" << nx << " " << ny << "\n255\n";

    PositionCamera cam(Vec3(-2, 2, 1), Vec3(0, 0, -1), Vec3(0, 1, 0), 90, float(nx) / float(ny));

    HitTable* list[5];
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5, new Lambertian(Vec3(0.8, 0.3, 0.3)));
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100, new Lambertian(Vec3(0.8, 0.8, 0.0)));
    list[2] = new Sphere(Vec3(1, 0, -1), 0.5, new Metal(Vec3(0.8, 0.6, 0.2)));
    list[3] = new Sphere(Vec3(-1, 0, -1), 0.5, new Dielectric(1.5));
    list[4] = new Sphere(Vec3(-1, 0, -1), -0.45, new Dielectric(1.5));
    HitTable* world = new HitTableList(list, 5);

    for (int j = ny - 1; j >= 0; j--)
    {
        for (int i = 0; i < nx; i++)
        {
            Vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                float u = float(i + random_double()) / float(nx);
                float v = float(j + random_double()) / float(ny);

                Ray ray(cam.origin, cam.lower_left_corner +
                    u * cam.horizontal + v * cam.vertical);

                col += MetallColor(ray, world, 0);

            }
            col /= float(ns);
            col = Vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);

            ofs << ir << " " << ig << " " << ib << "\n";
        }
    }
}

// Color Funktion musste in Main.cpp ausgelagert werden, weil der Struct aus der "HitTable.h" nicht in "Color.h" referenziert werden konnte. :(
 Vec3 color(const Ray& r, HitTable* world, int depth)
{
    // Erstellt ein leeres Struct aus der "HitTable.h", die den Punkt und den Vektor speichert, sowie das t.

    hit_record rec;

    // Using 0.001 for avoiding the shadow acne problem
    if (world->hit(r, 0.001, FLT_MAX, rec))
    {
        // Strahl f�r die "Verteilung" wird deklariert.
        Ray scattered;
        // Leerer Vektor "D�mpfung" wird deklariert.
        Vec3 attenuation;

        if (depth < 50 && rec.matr_ptr->scatter(r, rec, attenuation, scattered)) {
            // Rekursive Funktion. Gibt einen endg�ltigen Farbwert zur�ck.
            return attenuation*color(scattered, world, depth+1);
        }
        else {
            // Gibt einen schwarzen RGB-Wert zur�ck, falls die Bedingung False ist.
            return Vec3(0, 0, 0);
        }
    }
    else
    {
        // Hintergrund
        Vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5 * (unit_direction.y() + 1);
        // Beide Vektoren sind Farbwerte
        return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0);
    }
}


void test()
{
    // Image Width
    int nx = 200;

    // Image Height
    int ny = 100;

    int ns = 100;

    // Path of the "output.ppm" 
    std::ofstream ofs("./Output/output.ppm", std::ios::out);

    // Important first 3 lines of the ppm file
    //std::cout << "P3\n" << nx << " " << ny << "\n255\n";
    // Datei-Header der .ppm
    ofs << "P3\n" << nx << " " << ny << "\n255\n";


    /*
    //Erstelle Oberklasse HitTable Array mit zwei Elementen. Sir wird als Referenz der HitTableList �bergeben.
    HitTable* list[2];

    // In jedem Element werden jeweils zwei verschiedene Kugeln gespeichert, da Sphere (Kugel) die Unterklasse von HitTable ist.
    list[0] = new Sphere(Vec3(0, 0, -1), 0.5);
    list[1] = new Sphere(Vec3(0, -100.5, -1), 100);

    // Erstellt eine Liste, in der alle Objekte gespeichert sind, die sich in der Welt befinden.
    HitTable* world = new HitTableList(list, 2);
    */

    
    //Erstelle Oberklasse HitTable Array mit zwei Elementen. Sir wird als Referenz der HitTableList �bergeben.
    //HitTable* list[5];

    // In jedem Element werden jeweils zwei verschiedene Kugeln gespeichert, da Sphere (Kugel) die Unterklasse von HitTable ist.
    
    //list[0] = new Sphere(Vec3(0, 0, -1), 0.5, new Lambertian(Vec3(0.1, 0.2, 0.5)));
    //list[1] = new Sphere(Vec3(0, -100.5, -1), 100, new Lambertian(Vec3(0.8, 0.8, 0.0)));
    //list[2] = new Sphere(Vec3(1, 0, -1), 0.5, new Metal(Vec3(0.8, 0.6, 0.2), 1));
    //list[3] = new Sphere(Vec3(-1, 0, -1), -0.45, new Dielectric(1.5));
    //list[4] = new Sphere(Vec3(-1, 0, -1), 0.5, new Dielectric(1.5));

    //Camera Positionable Camera;
    PositionCamera cam(Vec3(-1, 0, 0), Vec3(0, 0, 1), Vec3(0, 1, 0), 90, float(nx) / float(ny));

    // Erstellt eine Liste, in der alle Objekte gespeichert sind, die sich in der Welt befinden.
    HitTable* list[2];
    float R = cos(M_PI/4);

    list[0] = new Sphere(Vec3(-R, 0, -1), R, new Lambertian(Vec3(0, 0, 1)));
    list[1] = new Sphere(Vec3(R, 0, -1), R, new Lambertian(Vec3(1, 0, 0)));


    //HitTable* world = new HitTableList(list, 5);
    HitTable* world = new HitTableList(list, 2);


    for (int j = ny - 1; j >= 0; j--) {
        for (int i = 0; i < nx; i++) {

            Vec3 col(0, 0, 0);
            for (int s = 0; s < ns; s++)
            {
                // u is the width
                float u = float(i + random_double()) / float(nx);

                //v is the height
                float v = float(j + random_double()) / float(ny);


                //Ray r = pinHoleCamera.get_ray(u, v);
                Ray r = cam.get_ray(u, v);

                col += color(r, world, 0);
            }
                col /= float(ns);

                col = Vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));

                int ir = int(255.99 * col[0]);
                int ig = int(255.99 * col[1]);
                int ib = int(255.99 * col[2]);
                std::cout << ir << " " << ig << " " << ib << "\n";
                ofs << ir << " " << ig << " " << ib << "\n";
 
        }
    }

    /*
    std::cout << RAND_MAX << "\n";
    std::cout << rand() << "\n";
    std::cout << rand() << "\n";
    std::cout << rand() << "\n";
    std::cout << rand() << "\n";
    std::cout << rand() << "\n";
    std::cout << Color::random_double() << "\n";
    std::cout << Color::random_double() << "\n";
    std::cout << Color::random_double() << "\n";
    std::cout << Color::random_double() << "\n";
    std::cout << Color::random_double() << "\n";
    */
}

int main()
{
    //feature_2_1_2_2();
    //feature_2_3();
    //feature_2_4();
    //feature_2_5();
    //feature_2_6();
    //feature_2_7();
    feature_2_8();
    //feature_2_9();
}